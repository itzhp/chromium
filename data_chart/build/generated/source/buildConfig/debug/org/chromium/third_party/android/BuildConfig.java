/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.chromium.third_party.android;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "org.chromium.third_party.android";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 17;
  public static final String VERSION_NAME = "73.0.3683.93";
}
