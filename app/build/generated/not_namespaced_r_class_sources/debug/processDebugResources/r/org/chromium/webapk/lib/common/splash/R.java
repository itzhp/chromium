/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package org.chromium.webapk.lib.common.splash;

public final class R {
    private R() {}

    public static final class color {
        private color() {}

        public static final int webapp_grey_900 = 0x7f060146;
        public static final int webapp_splash_title_light = 0x7f060147;
    }
    public static final class dimen {
        private dimen() {}

        public static final int webapp_splash_image_size_ideal = 0x7f0702b8;
        public static final int webapp_splash_image_size_minimum = 0x7f0702b9;
        public static final int webapp_splash_image_size_threshold = 0x7f0702ba;
        public static final int webapp_splash_large_title_margin_bottom = 0x7f0702bb;
        public static final int webapp_splash_offset = 0x7f0702bc;
        public static final int webapp_splash_small_image_size = 0x7f0702bd;
        public static final int webapp_splash_small_title_margin_top = 0x7f0702be;
    }
    public static final class id {
        private id() {}

        public static final int webapp_splash_screen_icon = 0x7f0b04bb;
        public static final int webapp_splash_screen_layout = 0x7f0b04bc;
        public static final int webapp_splash_screen_name = 0x7f0b04bd;
        public static final int webapp_splash_space = 0x7f0b04be;
    }
    public static final class layout {
        private layout() {}

        public static final int webapp_splash_screen_large = 0x7f0e01b0;
        public static final int webapp_splash_screen_no_icon = 0x7f0e01b1;
        public static final int webapp_splash_screen_small = 0x7f0e01b2;
    }
    public static final class style {
        private style() {}

        public static final int WebappSplashScreenText = 0x7f14026e;
        public static final int WebappSplashScreenTextTheme = 0x7f14026f;
    }
}
